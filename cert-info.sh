#!/bin/sh


# Author: Leroy Zwakman
# Description:
#   Script to get fast info about openssl cert,request and key
#   instead of typing every openssl command
# Usage:
#   cert-info.sh <filename>
# Changelog:
#   v0.1 20210328 Initial script

input=$1


function certificate() {

    # Show the essentials
    openssl x509 -noout -in $input \
        -subject \
        -issuer \
        -dates

    # skip the long parts of the key:
    openssl x509 -noout -in $input \
        -text \
        | grep -Pv '^\s+[a-f0-9\:]+:$'

}


function certificate_request() {

    openssl req -noout -in $input \
        -text

}

function private_key() {

    openssl rsa -noout -in $input \
        -text

}


if [[ `grep -F -e '-----BEGIN CERTIFICATE-----' $input` ]]; then
    certificate
    exit
fi

if [[ `grep -F -e '-----BEGIN CERTIFICATE REQUEST-----' $input` ]]; then
    certificate_request
    exit
fi


if [[ `grep -F -e '-----BEGIN RSA PRIVATE KEY-----' $input` ]]; then
    private_key
    exit
fi

if [[ `grep -F -e '-----BEGIN ENCRYPTED PRIVATE KEY-----' $input` ]]; then
    private_key
    exit
fi

echo "Unknown"
grep -F -e "-----" $input



