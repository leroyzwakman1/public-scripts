#!/bin/sh

# Author: Leroy Zwakman
# Description:
#   Look voor folders (categories),
#   check options within folder (files)
#    Type what is in the file

#  ~/.config/phrases/user
#    - username (containers leroy.zwakman)
#    - email (contains leroy.zwakman@domain.tld}

configfolder=~/.config/phrases

cd ${configfolder}

function showmenu() {

    _path=$1
    # decide wheather to show ..
    if [[ "`pwd`" == "${configfolder}" ]]; then
        chosen=$(ls -1p | sort| sort | dmenu -l 5)
    else
        chosen=$(ls -1pf | grep -v '^\.\/$' | sort| sort | dmenu -l 5)
    fi

    if [ ! $? -eq 0 ] ; then
        exit $?
    fi

    # check if dir, recurse
    if [ -d ${chosen} ]; then
        # enter directory, and regenerate menu
        cd ${chosen}
        showmenu ${chosen}
    # check if file; xdo!
    elif [ -f ${chosen} ]; then
        xdotool type --clearmodifiers --delay 0 "`cat ${chosen}`"
        exit $?
    # fail
    else
        echo "i don't know; sorry"
        exit 1
    fi

}

showmenu  ""
