#!/bin/sh

# Simple script to find a touchpad name,
# Find it's id, Check if enabled, toggle based on result

# find device name, limit result to 1, override if needed
DEVICE_NAME=`xinput list --name-only | grep -i Touch | head -1`

# find device id based on exact name
DEVICE_ID=`xinput list --id-only "${DEVICE_NAME}"`

# Check if device is enabled, gives 1 if enabled
DEVICE_ENABLED=`xinput list-props ${DEVICE_ID} \
       	| awk -F':' '/Device Enabled/{print $2}'`

# If enabled, disable
if [ ${DEVICE_ENABLED} -eq 1 ]; then
	xinput disable ${DEVICE_ID}
else
	xinput enable ${DEVICE_ID}
fi


