# Public Scripts

Scripts i wrote and cared to share :-)

### toggle-touchpad.sh
Simple script to quickly disable and enable mousepad,
possibly usefull for 'media keys'

e.g. for i3:
```
bindsym XF86TouchpadToggle exec /usr/local/bin/toggle-touchpad.sh # toggle touchpad

```

### phrases.sh
Script to select text to virtually type, idea based on autokey-gtk
can be used for longer to type commands like:
openssl x509 -noout -subject -dates -in

```
phrases/
├── me
│   ├── email
│   └── username
└── openssl
    └── cert-date
```

Set an shortkey to this script, and type to fill in by keyboard :)

